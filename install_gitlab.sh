#https://about.gitlab.com/install/#centos-7
sudo yum install -y curl policycoreutils-python openssh-server perl
# Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
sudo systemctl enable sshd
sudo systemctl start sshd
# Check if opening the firewall is needed with: sudo systemctl status firewalld
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --permanent --add-service=https
sudo systemctl reload firewalld
sudo yum install postfix
sudo systemctl enable postfix
sudo hostname localhost.localdomain.net
sudo systemctl start postfix
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
ipaddr=$(ip a | grep "192.168" | awk '{print $2}' | sed 's/\/24//')
sudo EXTERNAL_URL="http://$ipaddr" yum install -y gitlab-ee
